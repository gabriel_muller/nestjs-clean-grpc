FROM node:20-alpine as builder

WORKDIR /opt/api

COPY package.json ./
COPY pnpm-lock.yaml ./

RUN npm install -g npm@latest
RUN npm install -g pnpm @nestjs/cli @swc/cli @swc/core
RUN npm set registry http://npm.tecnospeed.local && pnpm install

COPY . .

RUN rm .swcrc && cp .build/.swcrc .swcrc

RUN pnpm build

RUN npm pkg delete scripts.prepare && pnpm prune --prod

FROM node:20-alpine as runner

WORKDIR /opt/api

COPY --from=builder /opt/api/node_modules ./node_modules
COPY --from=builder /opt/api/dist ./dist

CMD ["node", "dist/main"]