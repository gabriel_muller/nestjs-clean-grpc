import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ConfigModuleFactory } from '@/config/env.config';
import {
  ApiTokenModel,
  ApiTokenSchema,
} from '@/modules/auth/db/models/api-tokenmodel';
import { authRepositoryProviders } from '@/modules/auth/db/providers/repository.provider';

@Global()
@Module({
  imports: [
    ConfigModuleFactory.create(),
    MongooseModule.forFeature([
      { name: ApiTokenModel.name, schema: ApiTokenSchema },
    ]),
  ],
  providers: [...authRepositoryProviders],
  exports: [...authRepositoryProviders],
})
export class AuthModule {}
