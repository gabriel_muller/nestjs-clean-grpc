import { ApiToken } from '@/modules/auth/entities/api-token-entity';

export function mockApiTokenEntity(): ApiToken {
  return {
    id: 'mock-id',
    nome: 'mock-nome',
    token: 'mock-token',
    ativo: true,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
}
