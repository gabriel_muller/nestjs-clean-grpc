import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

/* c8 ignore start */
@Schema({ collection: 'api-tokens' })
export class ApiTokenModel {
  @Prop({ required: true })
  token: string;

  @Prop({ required: true })
  nome: string;

  @Prop({ default: true })
  ativo: boolean;

  @Prop({ default: Date.now, name: 'created_at' })
  createdAt: Date;

  @Prop({ default: Date.now, name: 'updated_at' })
  updatedAt: Date;
}
/* c8 ignore end */

export const ApiTokenSchema = SchemaFactory.createForClass(ApiTokenModel);
