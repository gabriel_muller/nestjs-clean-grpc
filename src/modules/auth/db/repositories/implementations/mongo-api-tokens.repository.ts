import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { ApiTokenModel } from '@/modules/auth/db/models/api-tokenmodel';
import { IApiTokesRepository } from '@/modules/auth/db/repositories/api-tokens.repository.interface';
import { ApiToken } from '@/modules/auth/entities/api-token-entity';

export class MongoApiTokensRepository implements IApiTokesRepository {
  constructor(
    /* c8 ignore next 2 */
    @InjectModel(ApiTokenModel.name)
    private readonly apiTokenModel: Model<ApiTokenModel>,
  ) {}

  async findByToken(token: string): Promise<ApiToken | null> {
    const existingToken = await this.apiTokenModel.findOne({ token });

    if (!existingToken) return null;

    return {
      id: existingToken.id.toString(),
      nome: existingToken.nome,
      token: existingToken.token,
      ativo: existingToken.ativo,
      createdAt: existingToken.createdAt,
      updatedAt: existingToken.updatedAt,
    };
  }
}
