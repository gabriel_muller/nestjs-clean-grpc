import { ExampleDocument } from '@/modules/health-check/entities/example-document.entity';

// PF: Função para mockar a entidade
// PF: Utilizada por testes e também por outros mocks
export function mockExampleDocumentEntity(): ExampleDocument {
  return {
    id: 'example-id',
    name: 'Example',
    description: 'Example description',
  };
}
