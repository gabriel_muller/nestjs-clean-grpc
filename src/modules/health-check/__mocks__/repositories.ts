import { mockExampleDocumentEntity } from '@/modules/health-check/__mocks__/entities';
import { IExampleDocumentsRepository } from '@/modules/health-check/db/repositories/example-documents.repository.interface';
import { ExampleDocument } from '@/modules/health-check/entities/example-document.entity';

// PF: Mock do repository para ser usado em testes unitários
// PF: O mock normalmente é feito com retorno de valores fixos, representando o caso de sucesso
// PF: Caso seja necessário alterar o retorno, deve se usar o spy durante o teste.
export class MockExampleDocumentsRepository
  implements IExampleDocumentsRepository
{
  async findById({ id }): Promise<ExampleDocument | null> {
    const mock = {
      ...mockExampleDocumentEntity(),
      id,
    };

    return {
      id: mock.id,
      name: mock.name,
      description: mock.description,
    };
  }
}
