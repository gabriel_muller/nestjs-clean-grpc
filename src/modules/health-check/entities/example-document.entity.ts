// PF: Entidade de dominio ou entidade de negocio
// PF: Representa uma entidade do mundo real
// PF: Não deve conter dependências de tecnologia, como por exemplo, decorators do MongoDB
export type ExampleDocument = {
  id: string;
  name: string;
  description: string;
};
