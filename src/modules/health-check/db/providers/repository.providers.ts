import { Provider } from '@nestjs/common';

import { HealthCheckReporitoryTokens } from '@/modules/health-check/db/providers/repository-tokens.enum';
import { MongoExampleDocumentsRepository } from '@/modules/health-check/db/repositories/implementations/mongo-example-documents.repository';

// PF: Lista de providers que serão injetados no módulo
export const healthCheckRepositoryProviders: Provider[] = [
  {
    provide: HealthCheckReporitoryTokens.EXAMPLE_DOCUMENTS_REPOSITORY,
    useClass: MongoExampleDocumentsRepository,
  },
];
