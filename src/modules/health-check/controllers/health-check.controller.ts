import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiExcludeEndpoint, ApiResponse, ApiTags } from '@nestjs/swagger';

import { APITokenProtected } from '@/modules/auth/decorators/api-token-auth.decorator';
import { GetStatusResponse } from '@/modules/health-check/controllers/dtos/get-status-response.dto';
import { GetStatusUsecase } from '@/modules/health-check/usecases/get-status.usecase';

@ApiTags('Health Check')
@Controller('status')
export class HealthCheckController {
  /* c8 ignore next 1 */
  constructor(private readonly healthCheckUsecase: GetStatusUsecase) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Status da API',
    type: GetStatusResponse,
  })
  async getStatus(): Promise<GetStatusResponse> {
    return this.healthCheckUsecase.execute();
  }

  @Get('/protected')
  @APITokenProtected()
  @ApiExcludeEndpoint()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Status da API',
    type: GetStatusResponse,
  })
  async getStatusProtetect(): Promise<GetStatusResponse> {
    return this.healthCheckUsecase.execute();
  }
}
