import { Provider } from '@nestjs/common';

import { AccountRepository } from '../repositories/implementations/account.repository';
import { AccountsRepositoryTokens } from './repository-tokens.enum';

export const accountRepositoryProviders: Provider[] = [
  {
    provide: AccountsRepositoryTokens.ACCOUNTS_REPOSITORY,
    useClass: AccountRepository,
  },
];
