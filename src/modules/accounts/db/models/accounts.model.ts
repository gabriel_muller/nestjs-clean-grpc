import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { HydratedDocument, ObjectId } from 'mongoose';

@Schema({ collection: 'accounts' })
export class AccountsModel {
  @Prop()
  id: ObjectId;

  @Prop()
  nome: string;

  @Prop()
  idade: number;

  @Prop()
  cidade: string;

  @Prop()
  bairro: string;
}

export type AccountsDocument = HydratedDocument<AccountsModel>;

export const AccountsSchema = SchemaFactory.createForClass(AccountsModel);
