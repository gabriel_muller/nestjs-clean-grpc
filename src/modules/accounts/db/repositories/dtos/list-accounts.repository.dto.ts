import { ListAccountsUsecaseResponseDTO } from '@/modules/accounts/usecases/list-accounts/dtos/list-accounts-by-id.usecase.dto';

export class ListAccountsRepositoryResponseDTO extends ListAccountsUsecaseResponseDTO {}
