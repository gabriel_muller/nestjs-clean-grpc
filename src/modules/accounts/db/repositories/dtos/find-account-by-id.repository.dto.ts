import {
  FindAccountByIdUsecaseParamsDTO,
  FindAccountByIdUsecaseResponseDTO,
} from '@/modules/accounts/usecases/find-account-by-id/dtos/find-account-by-id.usecase.dto';

export class FindAccountByIdRepositoryParamsDTO extends FindAccountByIdUsecaseParamsDTO {}

export class FindAccountByIdRepositoryResponseDTO extends FindAccountByIdUsecaseResponseDTO {}
