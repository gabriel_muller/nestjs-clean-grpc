import {
  CreateAccountUsecaseParamsDTO,
  CreateAccountUsecaseResponseDTO,
} from '@/modules/accounts/usecases/create-account/dtos/create-account.usecase.dto';

export class CreateAccountRepositoryParamsDTO extends CreateAccountUsecaseParamsDTO {}

export class CreateAccountRepositoryResponseDTO extends CreateAccountUsecaseResponseDTO {}
