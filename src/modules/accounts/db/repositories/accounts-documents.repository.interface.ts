import {
  CreateAccountRepositoryParamsDTO,
  CreateAccountRepositoryResponseDTO,
} from './dtos/create-account.repository.dto';
import {
  FindAccountByIdRepositoryParamsDTO,
  FindAccountByIdRepositoryResponseDTO,
} from './dtos/find-account-by-id.repository.dto';
import { ListAccountsRepositoryResponseDTO } from './dtos/list-accounts.repository.dto';

export interface IAccountsRepository {
  create(
    params: CreateAccountRepositoryParamsDTO,
  ): Promise<CreateAccountRepositoryResponseDTO>;
  findById(
    params: FindAccountByIdRepositoryParamsDTO,
  ): Promise<FindAccountByIdRepositoryResponseDTO | null>;
  list(): Promise<ListAccountsRepositoryResponseDTO[] | null>;
}
