import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ObjectId } from 'mongodb';
import { Model } from 'mongoose';

import { AccountsModel } from '../../models/accounts.model';
import { IAccountsRepository } from '../accounts-documents.repository.interface';
import {
  CreateAccountRepositoryParamsDTO,
  CreateAccountRepositoryResponseDTO,
} from '../dtos/create-account.repository.dto';
import {
  FindAccountByIdRepositoryParamsDTO,
  FindAccountByIdRepositoryResponseDTO,
} from '../dtos/find-account-by-id.repository.dto';
import { ListAccountsRepositoryResponseDTO } from '../dtos/list-accounts.repository.dto';

@Injectable()
export class AccountRepository implements IAccountsRepository {
  constructor(
    @InjectModel(AccountsModel.name)
    private readonly accountModel: Model<AccountsModel>,
  ) {}
  async create({
    nome,
    idade,
    cidade,
    bairro,
  }: CreateAccountRepositoryParamsDTO): Promise<CreateAccountRepositoryResponseDTO> {
    const accountToBeCreated = await this.accountModel.create({
      nome,
      idade,
      cidade,
      bairro,
    });

    return accountToBeCreated;
  }

  async findById({
    id,
  }: FindAccountByIdRepositoryParamsDTO): Promise<FindAccountByIdRepositoryResponseDTO> {
    const account = await this.accountModel.findOne({
      _id: new ObjectId(id),
    });
    return account;
  }

  async list(): Promise<ListAccountsRepositoryResponseDTO[]> {
    return await this.accountModel.find();
  }
}
