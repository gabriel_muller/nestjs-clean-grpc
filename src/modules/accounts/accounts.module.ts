import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AccountsController } from './controllers/accounts.controller';
import { AccountsModel, AccountsSchema } from './db/models/accounts.model';
import { accountRepositoryProviders } from './db/providers/repository.providers';
import { CreateAccountUsecase } from './usecases/create-account/create-account.usecase';
import { FindAccountByIdUsecase } from './usecases/find-account-by-id/find-account-by-id.usecase';
import { ListAccountsUsecase } from './usecases/list-accounts/list-accounts-by-id.usecase';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: AccountsModel.name, schema: AccountsSchema },
    ]),
  ],
  controllers: [AccountsController],
  providers: [
    ...accountRepositoryProviders,
    CreateAccountUsecase,
    FindAccountByIdUsecase,
    ListAccountsUsecase,
  ],
  exports: [...accountRepositoryProviders],
})
export class AccountModule {}
