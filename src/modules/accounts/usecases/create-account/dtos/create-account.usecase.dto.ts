export class CreateAccountUsecaseResponseDTO {
  nome: string;
  idade: number;
  cidade: string;
  bairro: string;
}

export class CreateAccountUsecaseParamsDTO {
  nome: string;
  idade: number;
  cidade: string;
  bairro: string;
}
