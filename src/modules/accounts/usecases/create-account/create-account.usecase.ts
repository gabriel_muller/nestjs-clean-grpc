import { Inject, Injectable } from '@nestjs/common';

import { AccountsRepositoryTokens } from '../../db/providers/repository-tokens.enum';
import { IAccountsRepository } from '../../db/repositories/accounts-documents.repository.interface';
import {
  CreateAccountUsecaseParamsDTO,
  CreateAccountUsecaseResponseDTO,
} from './dtos/create-account.usecase.dto';

@Injectable()
export class CreateAccountUsecase {
  constructor(
    @Inject(AccountsRepositoryTokens.ACCOUNTS_REPOSITORY)
    private readonly createAccountService: IAccountsRepository,
  ) {}
  async execute({
    nome,
    idade,
    cidade,
    bairro,
  }: CreateAccountUsecaseParamsDTO): Promise<CreateAccountUsecaseResponseDTO> {
    return await this.createAccountService.create({
      nome,
      idade,
      cidade,
      bairro,
    });
  }
}
