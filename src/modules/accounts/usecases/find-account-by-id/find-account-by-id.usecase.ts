import { Injectable, Inject } from '@nestjs/common';

import { AccountsRepositoryTokens } from '../../db/providers/repository-tokens.enum';
import { IAccountsRepository } from '../../db/repositories/accounts-documents.repository.interface';
import {
  FindAccountByIdUsecaseParamsDTO,
  FindAccountByIdUsecaseResponseDTO,
} from './dtos/find-account-by-id.usecase.dto';

@Injectable()
export class FindAccountByIdUsecase {
  constructor(
    @Inject(AccountsRepositoryTokens.ACCOUNTS_REPOSITORY)
    private readonly findAccountByIdService: IAccountsRepository,
  ) {}
  async execute({
    id,
  }: FindAccountByIdUsecaseParamsDTO): Promise<FindAccountByIdUsecaseResponseDTO> {
    const account = await this.findAccountByIdService.findById({
      id,
    });
    console.log(account);
    return account;
  }
}
