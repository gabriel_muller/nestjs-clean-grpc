export class FindAccountByIdUsecaseResponseDTO {
  nome: string;
  idade: number;
  cidade: string;
  bairro: string;
}

export class FindAccountByIdUsecaseParamsDTO {
  id: string;
}
