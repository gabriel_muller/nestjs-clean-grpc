import { Injectable, Inject } from '@nestjs/common';

import { AccountsRepositoryTokens } from '../../db/providers/repository-tokens.enum';
import { IAccountsRepository } from '../../db/repositories/accounts-documents.repository.interface';
import { ListAccountsUsecaseResponseDTO } from './dtos/list-accounts-by-id.usecase.dto';

@Injectable()
export class ListAccountsUsecase {
  constructor(
    @Inject(AccountsRepositoryTokens.ACCOUNTS_REPOSITORY)
    private readonly listAccountsService: IAccountsRepository,
  ) {}
  async execute(): Promise<ListAccountsUsecaseResponseDTO[]> {
    return await this.listAccountsService.list();
  }
}
