import { Controller, HttpCode, HttpStatus } from '@nestjs/common';
import { GrpcMethod, Payload } from '@nestjs/microservices';

import { CreateAccountUsecase } from '../usecases/create-account/create-account.usecase';
import { FindAccountByIdUsecase } from '../usecases/find-account-by-id/find-account-by-id.usecase';
import { ListAccountsUsecase } from '../usecases/list-accounts/list-accounts-by-id.usecase';
import {
  CreateAccountControllerParamsDTO,
  CreateAccountControllerResponseDTO,
} from './dtos/create-account.controller.dto';
import {
  FindAccountByIdControllerParamsDTO,
  FindAccountByIdControllerResponseDTO,
} from './dtos/find-account-by-id.controller.dto';
import { ListAccountsControllerResponseDTO } from './dtos/list-accounts.controller.dto';

@Controller('accounts')
export class AccountsController {
  /* c8 ignore next 1 */
  constructor(
    private readonly createAccountUsecase: CreateAccountUsecase,
    private readonly findAccountByIdUsecase: FindAccountByIdUsecase,
    private readonly listAccountsUsecase: ListAccountsUsecase,
  ) {}

  @GrpcMethod('AccountService')
  @HttpCode(HttpStatus.CREATED)
  async createAccount(
    @Payload()
    { nome, idade, cidade, bairro }: CreateAccountControllerParamsDTO,
  ): Promise<CreateAccountControllerResponseDTO> {
    return await this.createAccountUsecase.execute({
      nome,
      idade,
      cidade,
      bairro,
    });
  }

  @GrpcMethod('AccountService')
  @HttpCode(HttpStatus.OK)
  async findAccountById(
    @Payload() { id }: FindAccountByIdControllerParamsDTO,
  ): Promise<FindAccountByIdControllerResponseDTO> {
    const account = await this.findAccountByIdUsecase.execute({ id });
    console.log(account);
    return account;
  }

  @GrpcMethod('AccountService')
  @HttpCode(HttpStatus.OK)
  async listAccounts(): Promise<ListAccountsControllerResponseDTO[]> {
    return await this.listAccountsUsecase.execute();
  }
}
