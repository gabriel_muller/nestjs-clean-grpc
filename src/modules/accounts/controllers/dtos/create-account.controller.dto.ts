export class CreateAccountControllerResponseDTO {
  nome: string;
  idade: number;
  cidade: string;
  bairro: string;
}

export class CreateAccountControllerParamsDTO {
  nome: string;
  idade: number;
  cidade: string;
  bairro: string;
}
