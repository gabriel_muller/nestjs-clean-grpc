export class FindAccountByIdControllerResponseDTO {
  nome: string;
  idade: number;
  cidade: string;
  bairro: string;
}

export class FindAccountByIdControllerParamsDTO {
  id: string;
}
