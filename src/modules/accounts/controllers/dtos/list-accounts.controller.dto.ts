export class ListAccountsControllerResponseDTO {
  id: string;
  nome: string;
  idade: number;
  cidade: string;
  bairro: string;
}
