import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsOptional, Min } from 'class-validator';

import { SortingOrder } from '@/shared/enums/sorting-order.enum';

export class PaginationControllerFiltersDTO {
  @ApiPropertyOptional({
    description: 'Página atual',
  })
  @IsOptional()
  @Transform(({ value }) => Number(value))
  @Min(0, { message: 'O campo _page deve ser uma string numérica positiva' })
  _page?: number;

  @IsOptional()
  @ApiPropertyOptional()
  _sort?: string;

  @IsOptional()
  @ApiPropertyOptional()
  _order?: string;

  @IsOptional()
  @ApiPropertyOptional({
    description: `Quantidade de itens por página`,
  })
  @Transform(({ value }) => Number(value))
  @Min(0, { message: 'O campo _limit deve ser uma string numérica positiva' })
  _limit?: number;
}

export type PaginationFiltersDTO = {
  page?: number;
  limit?: number;
  order?: SortingOrder;
};
