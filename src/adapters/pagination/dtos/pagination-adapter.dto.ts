import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

class PaginationMetaDTO {
  @ApiProperty({ example: 10 })
  itemCount: number;

  @ApiProperty({ example: 50 })
  totalItems: number;

  @ApiProperty({ example: 10 })
  itemsPerPage: number;

  @ApiProperty({ example: 5 })
  totalPages: number;

  @ApiProperty({ example: 2 })
  currentPage: number;
}

class PaginationLinksDTO {
  @ApiProperty({ example: 'http://localhost:3000/exemplos?_limit=10&_page=1' })
  first: string;

  @ApiProperty({ example: 'http://localhost:3000/exemplos?_limit=10&_page=5' })
  last: string;

  @ApiPropertyOptional({
    example: 'http://localhost:3000/exemplos?_limit=10&_page=3',
  })
  next?: string;

  @ApiPropertyOptional({
    example: 'http://localhost:3000/exemplos?_limit=10&_page=1',
  })
  previous?: string;
}

export class PaginationAdapterDTO<T> {
  data: T[];

  @ApiProperty({ type: PaginationMetaDTO })
  meta: PaginationMetaDTO;

  @ApiProperty({ type: PaginationLinksDTO })
  links: PaginationLinksDTO;
}
