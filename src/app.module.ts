import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AccountModule } from './modules/accounts/accounts.module';
import { ConfigModuleFactory } from '@/config/env.config';
import { AuthModule } from '@/modules/auth/auth.module';
import { HealthCheckModule } from '@/modules/health-check/health-check.module';

@Module({
  imports: [
    ConfigModuleFactory.create(),
    MongooseModule.forRoot(process.env.MONGODB_URI),
    HealthCheckModule,
    AuthModule,
    AccountModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
