import { UserConfig, mergeConfig, defineConfig } from 'vitest/config';
import defaultConfig from './vite.config';

export default mergeConfig(
  defaultConfig as UserConfig,
  defineConfig({
    test: {
      include: ['src/**/*.spec.ts'],
    },
  }),
);
